import React, { useState, useEffect } from 'react';

import styles from './styles.module.css';

import Coms from './utils/coms';
import Editor from './pages/Editor';
import TabBar from './components/TabBar';
import Executor from './pages/Executor';

export default function App() {
	const [page, setPage] = useState("execute");

	useEffect(() => {
		/*Coms.send("ping", { foo: 'bar'}).then((results) => {
			console.log(results);
		});*/
	}, []);

	return (<div className={styles.appRoot}>
		<div>
			<TabBar
				tabs={[
					{id: 'editor',label: 'Editor'},
					{id: 'execute',label: 'Debug'},
				]}
				activeTab={page}
				setActiveTab={setPage}
			/>
		</div>
		<div style={{ flexGrow: 1 }}>
			{page === "editor" && <Editor />}
			{page === "execute" && <Executor />}
		</div>
	</div>);
}