import { STATE_CONDITIONAL, STATE_IF_END, STATE_IF_START, STATE_NUMBER, STATE_STRING_DOUBLE, STATE_VARIABLE, STATE_VAR_HAS_NAME } from "./states";

export function runStatement(statement, context) {
    switch (statement.state) {
        case STATE_VAR_HAS_NAME:
            const value = runStatement(statement.children[0], context);
            console.log(statement, value);
            const memoryLocation = addToMemory(value, context);
            context.variables[statement.name] = {
                memory: memoryLocation,
                type: value.subtype,
            };
            return "Variable " + statement.name + " was created. Where did the value come from?\n" + value.reason + "\nIt was placed in memory starting at location " + memoryLocation;
        case STATE_STRING_DOUBLE:
            return {
                type: 'value',
                subtype: 'string',
                value: statement.contents,
                reason: "String with value \"" + statement.contents + "\"",
            };
        case STATE_NUMBER:
            return {
                type: 'value',
                subtype: 'number',
                value: parseInt(statement.contents),
                reason: "Number with value \"" + statement.contents + "\"",
            };
        case STATE_CONDITIONAL:
            const leftValue = runStatement(statement.left_hand, context);
            const rightValue = runStatement(statement.children[0], context);

            console.log(leftValue, rightValue);
            const conditionalLeft = leftValue.value;
            const conditionalRight = rightValue.value;

            let matches = false;
            if (statement.operator === ">") {
                matches = conditionalLeft > conditionalRight;
            } else {
                throw new Error("Unknown operator " + statement.operator);
            }

            return {
                type: 'value',
                subtype: 'boolean',
                value: matches,
                reason: "Comparison between " + conditionalLeft + " " + statement.operator + " " + conditionalRight + " (result " + matches + ").\nLeft value came from:\n" + leftValue.reason + "\nRight value came from: \n" + rightValue.reason,
            };
        case STATE_VARIABLE:
            if (!context.variables[statement.variable]) {
                throw new Error("No such variable " + statement.variable);
            }
            const varData = context.variables[statement.variable];
            const variableValue = getFromMemory(context.memory, varData.memory, varData.type);

            return {
                type: 'value',
                subtype: varData.type,
                value: variableValue,
                reason: "Variable data from memory location " + varData.memory,
            };
        case STATE_IF_START:
            const conditionalResult = runStatement(statement.conditional, context);
            const valid = conditionalResult.value;
            if (!valid) {
                const newLine = context.activeLine + statement.highestIndex + 1;
                context.nextActiveLine = newLine;
                return "If statement, conditional is false. Jumps to line " + newLine + ". Reason:\n" + conditionalResult.reason;
            } else {
                return "If statement, conditional is true. Jumps to line " + (context.activeLine + 1) + ". Reason:\n" + conditionalResult.reason;
            }
        case STATE_IF_END:
            return "End of if block"
        default:
            throw new Error("Runner does not understand " + statement.state);
    }
}

function addToMemory(value, context) {
    let memoryStart;
    switch (value.type) {
        case "value":
            switch (value.subtype) {
                case "string":
                    // +1 for the null terminator
                    const length = value.value.length + 1;
                    memoryStart = findFreeMemory(context.memory, length);
                    if (memoryStart === -1) {
                        throw new Error("Can't find " + length + " bytes of free memory");
                    }

                    const valueArr = value.value.split("");
                    // null terminator
                    valueArr.push(0);
                    setMemory(context.memory, memoryStart, valueArr);
                    return memoryStart;
                case "number":
                    memoryStart = findFreeMemory(context.memory, 1);
                    if (memoryStart === -1) {
                        throw new Error("Can't find 1 byte of free memory");
                    }
                    setMemory(context.memory, memoryStart, [value.value]);
                    return memoryStart;
                case "boolean":
                    memoryStart = findFreeMemory(context.memory, 1);
                    if (memoryStart === -1) {
                        throw new Error("Can't find 1 byte of free memory");
                    }
                    setMemory(context.memory, memoryStart, [value.value ? 'true' : 'false']);
                    return memoryStart;
                default:
                    throw new Error("addToMemory can't handle value type of " + value.subtype);
            }
        default:
            throw new Error("addToMemory doesn't know how to handle type " + value.type);
    }
}

export function getStatementFromStatement(statement) {
    if (statement.index === 0) {
        return statement;
    } else {
        return statement.children[statement.index - 1];
    }
}

function findFreeMemory(memory, length) {
    let startPoint = null;
    for (let i=0;i<memory.length;i++) {
        const cell = memory[i];

        if (!cell.filled) {
            if (startPoint === null) {
                startPoint = i;
            } else {
                if (i - startPoint >= length) {
                    return startPoint;
                }
            }
        } else {
            startPoint = null;
        }
    }

    return -1;
}

function setMemory(memory, start, dataList) {
    for (let i=start;i<start+dataList.length;i++) {
        const cell = memory[i];
        cell.value = dataList[i-start];
        cell.filled = true;
    }
}

function getFromMemory(memory, start, type) {
    if (type === "number") {
        const val = memory[start];
        return val.value;
    }

    throw new Error("getFromMemory doesn't know how to handle " + type);
}