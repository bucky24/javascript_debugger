import {
    STATE_START,
    STATE_NUMBER,
    STATE_STRING_DOUBLE,
    STATE_VAR_HAS_NAME,
    STATE_VAR_START,
    STATE_VARIABLE,
    STATE_CONDITIONAL,
    STATE_IF_START,
    STATE_IF_END,
    STATE_FOR_START,
    STATE_VARIABLE_MATH,
    STATE_INCREMENT,
    STATE_FUNCTION_CALL,
    STATE_STRING_SINGLE,
    STATE_FOR_END,
} from "./states";

export default function compile(code) {
    console.log('compiling', code);

    const tokens = tokenize(code);
    console.log(tokens);

    const tree = buildTree(tokens);

    return tree;
}

function tokenize(code) {
    const breakpoints = [' ',';','\n', '=', '"', '(', ')', '\t', '<', '+', '\''];

    let buffer = "";
    const tokenList = [];
    for (let i=0;i<code.length;i++) {
        const char = code[i];

        if (breakpoints.includes(char)) {
            if (buffer.length > 0) {
                tokenList.push(buffer);
            }
            tokenList.push(char);
            buffer = "";
        } else {
            buffer += char;
        }
    }

    if (buffer.length > 0) {
        tokenList.push(buffer);
    }

    return tokenList.filter(token => token !== " ");
}

function buildTree(tokens) {
    const stateStack = [];

    let currentState = null;
    const statements = [];
    let currentLine = 0;

    const getNewState = () => {
        currentState = {
            state: STATE_START,
            line: currentLine,
        };
    }

    const pushState = () => {
        if (currentState !== null) {
            stateStack.push(currentState);
        }
        getNewState();
    }

    const unrollAll = () => {
        while (currentState !== null) {
            unrollState(false);
        }
    }

    const unrollState = (recreate = true) => {
        if (currentState) {
            // we have a parent
            if (stateStack.length > 0) {
                const parent = stateStack.pop();
                parent.children = [
                    ...parent.children || [],
                    currentState,
                ];
                currentState = parent;
            } else {
                // top level, add it as a statement
                if (currentState.state !== STATE_START) {
                    statements.push(currentState);
                }
                currentState = null;
            }
        }

        if (recreate && !currentState) {
            getNewState();
        }
    }

    getNewState();

    for (const token of tokens) {
        const state = currentState.state;
        console.log(`Processing ${token} with state ${state}`);

        if (token === "\n") {
            currentLine ++;
            currentState.line = currentLine;
        }

        if (state === STATE_START) {
            if (token === "var") {
                currentState.state = STATE_VAR_START;
                continue;
            } else if (token == "\"") {
                currentState.state = STATE_STRING_DOUBLE;
                currentState.contents = "";
                continue;
            } else if (token === "\n") {
                continue;
            } else if (parseInt(token) == token) {
                currentState.state = STATE_NUMBER;
                currentState.contents = token;
                continue;
            } else if (token === "if") {
                currentState.state = STATE_IF_START;
                continue;
            } else if (token === '\t') {
                continue;
            } else if (token === "for") {
                currentState.state = STATE_FOR_START;
                currentState.conditions = [];
                continue;
            } else if (token === "'") {
                currentState.state = STATE_STRING_SINGLE;
                currentState.content = "";
                continue;
            }
            
            else {
                currentState.state = STATE_VARIABLE;
                currentState.variable = token;
                continue;
            }
        } else if (state === STATE_VAR_START) {
            // assume token is a variable name
            currentState.name = token;
            currentState.state = STATE_VAR_HAS_NAME;
            continue;
        } else if (state === STATE_VAR_HAS_NAME) {
            if (token === "=") {
                // start processing value
                pushState(STATE_START);
                continue;
            } else if (token === ";") {
                unrollState();
                tokens.unshift(token);
                continue;
            }
        } else if (state === STATE_STRING_DOUBLE) {
            if (token === "\"") {
                // we're done
                unrollState();
                continue;
            }
            currentState.contents += token;
            continue;
        } else if (state === STATE_NUMBER) {
            if (token === ";") {
                // we're done
                unrollState();
                tokens.unshift(token);
                continue;
            } else if (token === ")") {
                unrollState();
                tokens.unshift(token);
                continue;
            }
        } else if (state === STATE_VARIABLE) {
            if (token === ">" || token === "<") {
                const newState = {
                    state: STATE_CONDITIONAL,
                    left_hand: currentState,
                    operator: token,
                };
                currentState = newState;
                pushState(STATE_START);
                continue;
            } else if (token === ")") {
                unrollState();
                tokens.unshift(token);
                continue;
            } else if (token === "+") {
                currentState.state = STATE_VARIABLE_MATH;
                currentState.symbol = "+";
                continue;
            } else if (token === "(") {
                currentState.state = STATE_FUNCTION_CALL;
                currentState.function = currentState.variable;
                continue;
            }
        } else if (state === STATE_CONDITIONAL) {
            if (token === ";") {
                unrollState();
                tokens.unshift(token);
                continue;
            } else if (token === ")") {
                unrollState();
                tokens.unshift(token);
                continue;
            }
        } else if (state === STATE_IF_START) {
            if (token === "(") {
                pushState(STATE_START);
                continue;
            } else if (token === ")") {
                currentState.conditional = currentState.children[0];
                currentState.children = [];
                continue;
            } else if (token === "{") {
                pushState(STATE_START);
                continue;
            } else if (token === "\n") {
                continue;
            } else if (token === "}") {
                currentState.children.push({ state: STATE_IF_END })
                unrollState();
                continue;
            }
        } else if (state === STATE_FOR_START) {
            if (token === "(") {
                pushState(STATE_START);
                continue;
            } else if (token === ";") {
                if (!currentState.hasCondition) {
                    currentState.conditions.push(currentState.children[0]);
                    currentState.children = [];
                    pushState(STATE_START);
                    continue;
                } else {
                    // some child statement just finished
                    continue;
                }
            } else if (token === ")") {
                currentState.conditions.push(currentState.children[0]);
                currentState.children = [];
                continue;
            } else if (token === "{") {
                currentState.hasCondition = true;
                pushState(STATE_START);
                continue;
            } else if (token === "\n") {
                continue;
            } else if (token === "}") {
                currentState.children.push({ state: STATE_FOR_END });
                unrollState();
                continue;
            }
        } else if (state === STATE_VARIABLE_MATH) {
            if (token === "+") {
                if (currentState.symbol === "+") {
                    currentState.state = STATE_INCREMENT;
                    continue;
                }
            }
        } else if (state === STATE_INCREMENT) {
            if (token === ")") {
                unrollState();
                tokens.unshift(token);
                continue;
            }
        } else if (state === STATE_FUNCTION_CALL) {
            if (token === ")") {
                unrollState();
                continue;
            } else {
                pushState(STATE_START);
                tokens.unshift(token);
                continue;
            }
        } else if (state === STATE_STRING_SINGLE) {
            if (token === "'") {
                unrollState();
                continue;
            } else {
                currentState.content += token;
                continue;
            }
        }
        throw new Error("CompileError: Unexpected token \"" + token + "\" on line " + (currentLine+1));
    }

    unrollAll();

    return statements;
}