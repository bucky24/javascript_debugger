import * as STATES from "./states";

export function generateCodeFromTree(tree) {
    const lines = [];

    for (const statement of tree) {
        const code = generateCodeFromStatement(statement);
        const newLines = code.split("\n");
        for (let i=0;i<newLines.length;i++) {
            lines.push({
                ...statement,
                code: newLines[i],
                index: i,
                highestIndex: newLines.length-1,
            });
        }
    }

    return lines;
}

function generateCodeFromStatement(statement) {
    let code;
    switch (statement.state) {
        case STATES.STATE_VAR_HAS_NAME:
            return "var " + statement.name + " = " + generateCodeFromStatement(statement.children[0]);
        case STATES.STATE_STRING_DOUBLE:
            return "\"" + statement.contents + "\"";
        case STATES.STATE_NUMBER:
            return statement.contents;
        case STATES.STATE_CONDITIONAL:
            const left = generateCodeFromStatement(statement.left_hand);
            const right = generateCodeFromStatement(statement.children[0]);

            return `${left} ${statement.operator} ${right}`;
        case STATES.STATE_VARIABLE:
            return statement.variable;
        case STATES.STATE_IF_START:
            code = "if (" + generateCodeFromStatement(statement.conditional) + ") {\n";

            statement.children.forEach((statement) => {
                const childCode = generateCodeFromStatement(statement);
                if (childCode === "") {
                    return;
                }
                code += "\t" + generateCodeFromStatement(statement) + ";\n";
            });
            code += "}";
            return code;
        case STATES.STATE_IF_END:
            return '';
        case STATES.STATE_FOR_START:
            code = "for (" + statement.conditions.map((condition) => generateCodeFromStatement(condition)).join("; ") + ") {\n";
            code += statement.children.map((child) => generateCodeFromStatement(child)).map((code, index, list) => {
                if (index === list.length-1) {
                    return code;
                } else {
                    return "\t" + code;
                }
            }).join(";\n");

            return code;
        case STATES.STATE_INCREMENT:
            return statement.variable + "++";
        case STATES.STATE_FUNCTION_CALL:
            return statement.function + "(" + statement.children.map((child) => generateCodeFromStatement(child)).join(",") + ")";
        case STATES.STATE_STRING_SINGLE:
            return "'" + statement.content + "'";
        case STATES.STATE_FOR_END:
            return "}"
        default:
            throw new Error("Unexpected state " + statement.state);
    }
    return "code";
}