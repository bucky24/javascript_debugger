import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import { CodeProvider } from './contexts/CodeContext';
import { ExecutionProvider } from './contexts/ExecutionContext';

ReactDOM.render(
    <CodeProvider>
        <ExecutionProvider>
            <App />
        </ExecutionProvider>
    </CodeProvider>
, document.getElementById('root'));
