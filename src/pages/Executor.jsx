import { useContext } from "react";
import CodeContext from "../contexts/CodeContext";
import ExecutionContext from "../contexts/ExecutionContext";

export default function Executor() {
    const { tree, error } = useContext(CodeContext);
    const { activeLine, execute, memory, variables, reason, error: executionError } = useContext(ExecutionContext);

    const reasonLines = reason.split("\n");

    return <div style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
            <h3>Executor</h3>
            <div style={{ marginLeft: 10 }}>
                <button onClick={execute}>Execute Line</button>
            </div>
            {error && <div style={{marginLeft: 10}}>Error: {error}</div>}
            {executionError && <div style={{marginLeft: 10}}>Error: {executionError}</div>}
        </div>
        <div
            style={{
                flexGrow: 1,
                display: 'flex',
            }}
        >
            <div style={{flexGrow: 2 }}>
                {tree.map((statement, index) => {
                    return <div style={{
                        display: 'flex',
                        backgroundColor: activeLine === index && "green",
                        color: activeLine === index && "white",
                        fontSize: 30,
                    }}>
                        <div style={{ width: 25 }}>{index}</div>
                        <pre style={{ margin: 0 }}>
                            {statement.code}
                        </pre>
                    </div>
                })}
            </div>
            <div style={{
                flexGrow: 1,
                display: 'flex',
                flexDirection: 'column',
                marginLeft: 10,
                maxWidth: 400,
            }}>
                <div style={{ flexGrow: 1}}>
                    <h2>Code Explaination</h2>
                    {reasonLines.map((line) => {
                        return <div>{line}</div>;
                    })}
                </div>
                <div style={{ flexGrow: 1, maxHeight: '50vh', overflow: 'auto' }}>
                    <h2>Memory</h2>
                    <table>
                        <thead>
                            <tr>
                                <td>Location</td>
                                <td>Value</td>
                                <td>Set</td>
                            </tr>
                        </thead>
                        <tbody>
                            {memory.map((cell, index) => {
                                return <tr>
                                    <td style={{ fontSize: 30 }}>{index}</td>
                                    <td style={{ fontSize: 30 }}>{cell.value}</td>
                                    <td style={{ fontSize: 30 }}>{cell.filled ? "Yes" : ""}</td>
                                </tr>
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
            <div style={{
                flexGrow: 1,
                display: 'flex',
                flexDirection: 'column',
                marginLeft: 10,
            }}>
                <h2>Variable Table</h2>
                <table>
                    <thead>
                        <tr>
                            <td>Variable</td>
                            <td>Type</td>
                            <td>Memory Location</td>
                        </tr>
                    </thead>
                    <tbody>
                        {Object.keys(variables).map((variable) => {
                            return <tr>
                                <td>{variable}</td>
                                <td>{variables[variable].type}</td>
                                <td>{variables[variable].memory}</td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    </div>;
}