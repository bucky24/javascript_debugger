import { useContext, useEffect } from "react";

import CodeContext from "../contexts/CodeContext";


export default function Editor() {
    const { code, setCode, error } = useContext(CodeContext);

    return <div style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
            <h3>Code Editor</h3>
            {error && <div style={{marginLeft: 10}}>Error: {error}</div>}
        </div>
        <textarea
            style={{
                flexGrow: 1,
            }}
            value={code}
            onChange={(e) => setCode(e.target.value)}
        ></textarea>
    </div>;
}