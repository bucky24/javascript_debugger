export default function TabBar({ tabs, activeTab, setActiveTab }) {
    return <div style={{
        display: 'flex',
    }}>
        {tabs.map((tab) => {
            const active = activeTab === tab.id;
            return <div
                key={`tab_${tab.id}`}
                style={{
                    cursor: 'pointer',
                    padding: 10,
                    backgroundColor: active ? 'green' : 'red',
                    marginRight: 10,
                }}
                onClick={() => {
                    setActiveTab(tab.id);
                }}
            >
                {tab.label}
            </div>
        })}
    </div>
}