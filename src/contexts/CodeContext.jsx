import React, { useEffect, useState } from 'react';
import compile from '../utils/compiler';
import { generateCodeFromTree } from '../utils/generator';

const CodeContext = React.createContext();
export default CodeContext;

export function CodeProvider({ children }) {
    const [code, setCode] = useState("for (var i=0;i<5;i++) {\n\tconsole.log('loop');\n}");
    const [tree, setTree] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        try {
            const tree = compile(code);
            const generated = generateCodeFromTree(tree);
            setTree(generated);
            //console.log(generated);
            setError(null);
        } catch (error) {
            if (error.message.startsWith("Compile Error:")) {
                setError(error.message);
            } else {
                console.error(error);
            }
        }
    }, [code]);
    
    const value = {
        code,
        setCode,
        tree,
        error,
    };

    return <CodeContext.Provider value={value}>
        {children}
    </CodeContext.Provider>
}