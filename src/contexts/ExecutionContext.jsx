import React, { useContext, useRef, useState, useEffect } from 'react';
import CodeContext from './CodeContext';
import { runStatement, getStatementFromStatement } from '../utils/runtime';

const ExecutionContext = React.createContext();
export default ExecutionContext;

export function ExecutionProvider({ children }) {
    const { tree } = useContext(CodeContext);
    const contextRef = useRef({ memory: [], variables: {}, activeLine: -1});
    const [reason, setReason] = useState("");
    const [error, setError] = useState(null);

    useEffect(() => {
        contextRef.current.memory = [];
        // start with 20 entries
        for (let i=0;i<20;i++) {
            contextRef.current.memory.push({
                value: 0,
                filled: false,
            });
        }
    }, []);

    const value = {
        activeLine: contextRef.current.activeLine,
        execute: () => {
            if (contextRef.current.nextActiveLine) {
                contextRef.current.activeLine = contextRef.current.nextActiveLine;
                contextRef.current.nextActiveLine = null;
            } else {
                contextRef.current.activeLine ++;
            }

            if (contextRef.current.activeLine < tree.length) {
                const statement = getStatementFromStatement(tree[contextRef.current.activeLine]);
                try {
                    const reason = runStatement(statement, contextRef.current);
                    setReason(reason);
                } catch(error) {
                    setError(error.message);
                }
            } else {
                setReason("Program ended");
            }
        },
        memory: contextRef.current.memory,
        variables: contextRef.current.variables,
        reason,
        error,
    };

    return <ExecutionContext.Provider value={value}>
        {children}
    </ExecutionContext.Provider>
}